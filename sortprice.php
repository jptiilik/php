<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>PHP</title>
</head>
<body>
  <?php
  class Products
  {
    public static function sortByPriceAscending($jsonString)
    {
        $shoppingList = json_decode($jsonString);

        function cmp($a, $b) {
          return strcmp($a->price, $b->price);
        }
        usort($shoppingList, "cmp");

        return json_encode($shoppingList);

    }
  }

  echo Products::sortByPriceAscending('[{"name":"eggs","price":1},{"name":"coffee","price":9.99},{"name":"rice","price":4.04}]');
  ?>
</body>
</html>
