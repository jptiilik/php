<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>PHP</title>
</head>
<body>
<?php

class Programmer {

  public $languages;

  public function __construct() {
    $this->languages=[];
  }

  public function addLanguage($language) {
    $this->languages[] = $language;
  }

  public function getLanguages() {
    return $this->languages;
  }

}

class ProgrammerTeacher extends Programmer {

  public function teach(Programmer $programmer, $language) {

    if (!in_array($language, $programmer->getLanguages())) {
      $programmer->addLanguage($language);
    }

  }
}

/* test cases */

$programmer = new Programmer();
$programmer->addLanguage('PHP'); // teach himself

$teacher = new ProgrammerTeacher();
$teacher->teach($programmer, 'suomi');
$teacher->teach($programmer, 'savo');
$teacher->teach($programmer, 'savo'); // already knows

echo "<pre>Programmer:";
print_r($programmer->getLanguages());
echo "</pre>";

$teacher2 = new ProgrammerTeacher();
$teacher2->teach($teacher, 'savo'); // teacher to teacher
echo "<pre>Teacher:";
print_r($teacher->getLanguages());
echo "</pre>";
?>

</body>
</html>
