<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>PHP</title>
</head>
<body>
<?php
class ArraySum
{
    public static function appendSum(&$array) // pass variable by reference
    {
        $sum = array_sum($array);
        $array[] = $sum;
        return $sum;
    }
}

/* test */
$array = array(1,2,3);
$sum = ArraySum::appendSum($array);
echo "<pre>";
print_r($array);
echo($sum.PHP_EOL);
?>

</body>
</html>
